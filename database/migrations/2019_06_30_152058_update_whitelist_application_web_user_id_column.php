<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateWhitelistApplicationWebUserIdColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('whitelist_applications', function (Blueprint $table) {
            $table->renameColumn('webUserId', 'web_user_id');
            $table->renameColumn('identifier', 'user_identifier');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('whitelist_applications', function (Blueprint $table) {
            $table->renameColumn('web_user_id', 'webUserId');
            $table->renameColumn('user_identifier', 'identifier');
        });
    }
}
