<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddComServTime extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fine_types', function (Blueprint $table) {
            $table->integer('com_service_min');
            $table->integer('com_service_max');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fine_types', function (Blueprint $table) {
            $table->dropColumn('com_service_min');
            $table->dropColumn('com_service_max');
        });
    }
}
