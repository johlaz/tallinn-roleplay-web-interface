@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header justify-content-center">{{__('headers.ems_job_application')}}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('jobApplicationStore') }}">
                            @csrf
                            <input type="hidden" name="job" value="ems">
                            <div class="form-row border-bottom border-light">
                                <div class="form-group col-md-6">
                                    <label for="discordName">{{ __('jobApplications.discordName') }}</label>
                                    <input type="text"
                                           class="form-control {{ $errors->has('discordName') ? ' is-invalid' : '' }}"
                                           id="discordName" placeholder="{{ __('jobApplications.discordName') }}"
                                           name="discordName"
                                           value="{{ old('discordName') }}" required autofocus>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="age">{{ __('jobApplications.age') }}</label>
                                    <input type="number"
                                           class="form-control {{ $errors->has('age') ? ' is-invalid' : '' }}"
                                           id="age" placeholder="{{ __('jobApplications.age') }}" name="age"
                                           value="{{ old('age') }}" required autofocus>
                                </div>
                            </div>
                            <div class="form-row border-bottom border-light">
                                <div class="form-group col-md-6">
                                    <p>{{ __('jobApplications.havePlayedRp') }}</p>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="havePlayedRp"
                                               id="havePlayedRpFewDays" value="{{ __('jobApplications.few_days') }}">
                                        <label class="form-check-label" for="havePlayedRpFewDays">
                                            {{ __('jobApplications.few_days') }}
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="havePlayedRp"
                                               id="havePlayedRpFirstTime"
                                               value="{{ __('jobApplications.first_time') }}">
                                        <label class="form-check-label" for="havePlayedRpFirstTime">
                                            {{ __('jobApplications.first_time') }}
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="havePlayedRp"
                                               id="havePlayedRpOneMonth" value="{{ __('jobApplications.one_month') }}">
                                        <label class="form-check-label" for="havePlayedRpOneMonth">
                                            {{ __('jobApplications.one_month') }}
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="havePlayedRp"
                                               id="havePlayedRpTwoMonths"
                                               value="{{ __('jobApplications.two_months') }}">
                                        <label class="form-check-label" for="havePlayedRpTwoMonths">
                                            {{ __('jobApplications.two_months') }}
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="havePlayedRp"
                                               id="havePlayedRpLongerThenTwoMonths"
                                               value="{{ __('jobApplications.longer_then_two_months') }}">
                                        <label class="form-check-label" for="havePlayedRpLongerThenTwoMonths">
                                            {{ __('jobApplications.longer_then_two_months') }}
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <p>{{ __('jobApplications.playingTimes') }}</p>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="playingTimes"
                                               id="playingEveryDay" value="{{ __('jobApplications.every_day') }}"
                                               checked>
                                        <label class="form-check-label" for="playingEveryDay">
                                            {{ __('jobApplications.every_day') }}
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="playingTimes"
                                               id="playingAfterWork"
                                               value="{{ __('jobApplications.every_day_after_work_school') }}">
                                        <label class="form-check-label" for="playingAfterWork">
                                            {{ __('jobApplications.every_day_after_work_school') }}
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="playingTimes"
                                               id="playingWeekends" value="{{ __('jobApplications.only_weekends') }}">
                                        <label class="form-check-label" for="playingWeekends">
                                            {{ __('jobApplications.only_weekends') }}
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="playingTimes"
                                               id="playingOther"
                                               value="{{ __('jobApplications.cant_answer_depends_on_how_much_time_I_have_after_work_and_personal_life') }}">
                                        <label class="form-check-label" for="playingOther">
                                            {{ __('jobApplications.cant_answer_depends_on_how_much_time_I_have_after_work_and_personal_life') }}
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row border-bottom border-light">
                                <div class="form-group col-md-6">
                                    <p>{{ __('jobApplications.irlExperienceEms') }}</p>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="irlExperienceEms"
                                               id="irlExperienceYes" value="{{ __('jobApplications.true') }}">
                                        <label class="form-check-label" for="irlExperienceYes">
                                            {{ __('jobApplications.true') }}
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="irlExperienceEms"
                                               id="irlExperienceNo" value="{{ __('jobApplications.false') }}" checked>
                                        <label class="form-check-label" for="irlExperienceNo">
                                            {{ __('jobApplications.false') }}
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <p>{{ __('jobApplications.emsRpExperience') }}</p>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="emsRpExperience"
                                               id="emsRpExperienceYes" value="{{ __('jobApplications.true') }}">
                                        <label class="form-check-label" for="emsRpExperienceYes">
                                            {{ __('jobApplications.true') }}
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="emsRpExperience"
                                               id="emsRpExperienceNo" value="{{ __('jobApplications.false') }}" checked>
                                        <label class="form-check-label" for="emsRpExperienceNo">
                                            {{ __('jobApplications.false') }}
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row border-bottom border-light">
                                <div class="form-group col-md-6">
                                    <p>{{ __('jobApplications.sirensAllowed') }}</p>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="sirensAllowed"
                                               id="sirensAllowedYes" value="{{ __('jobApplications.true') }}">
                                        <label class="form-check-label" for="sirensAllowedYes">
                                            {{ __('jobApplications.true') }}
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="sirensAllowed"
                                               id="sirensAllowedNo" value="{{ __('jobApplications.false') }}">
                                        <label class="form-check-label" for="sirensAllowedNo">
                                            {{ __('jobApplications.false') }}
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <p>{{ __('jobApplications.criminalActions') }}</p>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="criminalActions"
                                               id="criminalActionsYes" value="{{ __('jobApplications.true') }}">
                                        <label class="form-check-label" for="criminalActionsYes">
                                            {{ __('jobApplications.true') }}
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="criminalActions"
                                               id="criminalActionsNo" value="{{ __('jobApplications.false') }}">
                                        <label class="form-check-label" for="criminalActionsNo">
                                            {{ __('jobApplications.false') }}
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row border-bottom border-light">
                                <div class="form-group col-md-6">
                                    <p>{{ __('jobApplications.chitChat') }}</p>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="chitChat"
                                               id="chitChatDiscord" value="{{ __('jobApplications.in_discord') }}">
                                        <label class="form-check-label" for="chitChatDiscord">
                                            {{ __('jobApplications.in_discord') }}
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="chitChat" id="chitChatGame"
                                               value="{{ __('jobApplications.in_game') }}">
                                        <label class="form-check-label" for="chitChatGame">
                                            {{ __('jobApplications.in_game') }}
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row border-bottom border-light">
                                <div class="form-group col-md-6">
                                    <label
                                        for="characterDescription">{{ __('jobApplications.characterDescription') }}</label>
                                    <textarea
                                        class="form-control {{ $errors->has('characterDescription') ? ' is-invalid' : '' }}"
                                        id="characterDescription"
                                        placeholder="{{ __('jobApplications.characterDescription') }}"
                                        name="characterDescription" required autofocus>
                                            {{ old('characterDescription') }}
                                    </textarea>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="whyYouWantToBeEms">{{ __('jobApplications.whyYouWantToBeEms') }}</label>
                                    <textarea
                                        class="form-control {{ $errors->has('whyYouWantToBeEms') ? ' is-invalid' : '' }}"
                                        id="whyYouWantToBeEms"
                                        placeholder="{{ __('jobApplications.whyYouWantToBeEms') }}"
                                        name="whyYouWantToBeEms" required autofocus>
                                            {{ old('whyYouWantToBeEms') }}
                                    </textarea>
                                </div>
                            </div>
                            <div class="form-row border-bottom border-light">
                                <div class="form-group col-md-6">
                                    <label for="futurePlans">{{ __('jobApplications.futurePlans') }}</label>
                                    <input type="text"
                                           class="form-control {{ $errors->has('futurePlans') ? ' is-invalid' : '' }}"
                                           id="futurePlans" placeholder="{{ __('jobApplications.futurePlans') }}"
                                           name="futurePlans" value="{{ old('futurePlans') }}"
                                           required autofocus>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="whyYouWantToBeEms">{{ __('jobApplications.numberOfJobGrades') }}</label>
                                    <input type="number"
                                           class="form-control {{ $errors->has('numberOfJobGrades') ? ' is-invalid' : '' }}"
                                           id="numberOfJobGrades"
                                           placeholder="{{ __('jobApplications.numberOfJobGrades') }}"
                                           name="numberOfJobGrades" value="{{ old('numberOfJobGrades') }}"
                                           required autofocus>
                                </div>
                            </div>
                            <div class="form-row border-bottom border-light">
                                <div class="form-group col-md-12">
                                    <label for="situation1">
                                        {{ __('jobApplications.situation1') }}
                                    </label>
                                    <textarea
                                        class="form-control {{ $errors->has('situation1') ? ' is-invalid' : '' }}"
                                        id="situation1" placeholder="{{ __('jobApplications.describe_your_actions') }}"
                                        name="situation1"
                                        required autofocus>
                                        {{ old('situation1') }}
                                    </textarea>
                                </div>
                            </div>

                            <input type="submit" class="btn btn-success" id="confirmButton"
                                   value="{{__('buttons.submit_application')}}" onclick="overlayOn()" disabled>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<script>
    window.onload = function () {
        const discordNameField = document.getElementById('discordName');
        const ageField = document.getElementById('age');
        const characterDescriptionField = document.getElementById('characterDescription');
        const whyYouWantToBeEmsField = document.getElementById('whyYouWantToBeEms');
        const futurePlansField = document.getElementById('futurePlans');
        const numberOfJobGradesField = document.getElementById('numberOfJobGrades');
        const situation1Field = document.getElementById('situation1');
        const confirmButton = document.getElementById('confirmButton');

        let elementArray = [
            discordNameField,
            ageField,
            characterDescriptionField,
            whyYouWantToBeEmsField,
            futurePlansField,
            numberOfJobGradesField,
            situation1Field
        ];

        elementArray.forEach(function (element) {
            element.addEventListener('keyup', function (event) {
                let isValidDiscordNameField = discordNameField.checkValidity();
                let isValidAgeField = ageField.checkValidity();
                let isValidCharacterDescriptionField = characterDescriptionField.checkValidity();
                let isValidWhyYouWantToBeEmsField = whyYouWantToBeEmsField.checkValidity();
                let isValidFuturePlansField = futurePlansField.checkValidity();
                let isValidNumberOfJobGradesField = numberOfJobGradesField.checkValidity();
                let isValidSituation1Field = situation1Field.checkValidity();

                if (
                    isValidDiscordNameField
                    && isValidAgeField
                    && isValidCharacterDescriptionField
                    && isValidCharacterDescriptionField
                    && isValidWhyYouWantToBeEmsField
                    && isValidFuturePlansField
                    && isValidNumberOfJobGradesField
                    && isValidSituation1Field
                ) {
                    confirmButton.disabled = false;
                } else {
                    confirmButton.disabled = true;
                }
            });
        });
    }
</script>
