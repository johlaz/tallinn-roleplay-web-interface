@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>Create a new fine</h2>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="card">
            <div class="card-header">
                <strong>Legend of category:</strong>
            </div>
            <div class="card-body">
                <ul>
                    <li>0 - traffic fines</li>
                    <li>1 - minor offence</li>
                    <li>2 - average offence</li>
                </ul>
                <form method="POST" action="{{route($submitRoute)}}">
                    @csrf
                    <div class="form-row">
                        <div class="form-group col-md-8">
                            <label for="label">Fine Description</label>
                            <input class="form-control" id="label" name="label"
                                   value="@if($action === 'update') {{$fine->label}} @endif" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-2">
                            <label for="amount">Fine Amount</label>
                            <input class="form-control" id="amount" name="amount"
                                   value="@if($action === 'update') {{$fine->amount}} @endif" required>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="amount">Category</label>
                            <input class="form-control" id="category" name="category"
                                   value="@if($action === 'update') {{$fine->category}} @endif" required>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="minJail">Min Jail Time</label>
                            <input class="form-control" id="minJail" name="minJail"
                                   value="@if($action === 'update') {{$fine->min_jail}} @endif" required>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="maxJail">Max Jail Time</label>
                            <input class="form-control" id="maxJail" name="maxJail"
                                   value="@if($action === 'update') {{$fine->max_jail}} @endif" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-2">
                            <label for="comServiceMin">Min Community Service / ÜKT</label>
                            <input class="form-control" id="comServiceMin" name="comServiceMin"
                                   value="@if($action === 'update') {{$fine->com_service_min}} @endif" required>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="comServiceMax">Max Community Service / ÜKT</label>
                            <input class="form-control" id="comServiceMax" name="comServiceMax"
                                   value="@if($action === 'update') {{$fine->com_service_max}} @endif" required>
                        </div>
                    </div>
                    @if($action === 'update')
                        <input type="hidden" value="{{$fine->id}}" name="id">
                    @endif
                    <div class="form-row">
                        <button type="submit" class="btn btn-info ml-1" onclick="overlayOn()">
                            @if($action === 'update')
                                {{__('buttons.edit_fine')}}
                            @else
                                {{__('buttons.add_fine')}}
                            @endif
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
