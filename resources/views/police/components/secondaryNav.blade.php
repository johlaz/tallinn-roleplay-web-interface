<ul class="nav nav-tabs nav-tabs-dark">
    <li class="nav-item">
        <a @if(\Illuminate\Support\Facades\Route::currentRouteName() === 'policeSingleUser') class="nav-link-dark active-dark" @else class="nav-link-dark" @endif
            href="{{route('policeSingleUser', ['steamId' => $identifier])}}" onclick="overlayOn()">
            {{__('link.user_data')}}
        </a>
    </li>
    <li class="nav-item">
        <a @if(\Illuminate\Support\Facades\Route::currentRouteName() === 'policeSingleUserCars') class="nav-link-dark active-dark" @else class="nav-link-dark" @endif
            href="{{route('policeSingleUserCars', ['steamId' => $identifier])}}" onclick="overlayOn()">
            {{__('link.user_cars')}}
        </a>
    </li>
</ul>
