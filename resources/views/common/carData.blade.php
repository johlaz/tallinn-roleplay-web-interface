@extends('layouts.app') @section('content')
<div class="container">
	<div class="card">
		<div class="card-header">
			<b>{{__('headers.car_data')}}</b>
		</div>
		@if (session('status'))
		<div class="alert alert-success" role="alert">{{ session('status') }}
		</div>
		@endif @if (session('error'))
		<div class="alert alert-danger" role="alert">{{ session('error') }}</div>
		@endif
		<div class="card-body">
			<div class="text-black-50">
				@if( !Auth::guest() and (
				\Illuminate\Support\Facades\Auth::user()->hasRole('admin') or
				in_array(
				\Illuminate\Support\Facades\Auth::user()->getFiveMUserData()->job,
				\App\Http\Controllers\PoliceController::ALLOWED_JOBS ) ) )
				<p
					onclick="
                        window.location='{{route('policeSingleUser', [$car->owner])}}';
                        overlayOn();"
					class="mouse-over">
					<strong>{{__('texts.owner_name')}}:</strong>
					{{\App\Helpers\UserHelper::getCharacterName($user)}}
				</p>
				@else
				<p>
					<strong>{{__('texts.owner_name')}}:</strong>
					{{\App\Helpers\UserHelper::getCharacterName($user)}}
				</p>
				@endif
				<p>
					<strong>{{__('texts.car_plate')}}</strong> {{$car->plate}}
				</p>
				<p>
					<strong>{{ __('texts.car_model') }}</strong> @isset(\App\CarModel::where('code',
					json_decode($car->vehicle)->model)->first()->name)
					{{\App\CarModel::where('code',
					json_decode($car->vehicle)->model)->first()->name}} @endisset
				</p>
				<p>
					<strong>{{ __('texts.car_reg_date') }}</strong> {{ (new DateTime($car->reg_date))->format('d.m.Y') }}
				</p>
				@if( !isset(\App\CarInsurance::where('plate',
				$car->plate)->first()->is_insured) or
				!(\App\CarInsurance::where('plate',
				$car->plate)->first()->is_insured) or new
				DateTime(\App\CarInsurance::where('plate',
				$car->plate)->first()->end_time) < new DateTime() )
				<div class="text-danger">
					@else
					<div class="text-success">
						@endif
						<p>
							<strong>{{ __('texts.is_insured') }}:</strong> @if(
							isset(\App\CarInsurance::where('plate',
							$car->plate)->first()->is_insured) and
							isset(\App\CarInsurance::where('plate',
							$car->plate)->first()->end_time) and
							\App\CarInsurance::where('plate',
							$car->plate)->first()->is_insured and new
							DateTime(\App\CarInsurance::where('plate',
							$car->plate)->first()->end_time) > new DateTime() )
								{{ __('texts.insurance_yes') }}
						 	@else
							 	{{ __('texts.insurance_no') }}
							@endif
						</p>
						<p>
							<strong>{{ __('texts.insurance_end_date') }}:</strong>
							@if(isset(\App\CarInsurance::where('plate',
							$car->plate)->first()->end_time))
							{{\App\CarInsurance::where('plate',
							$car->plate)->first()->end_time}} @endif
						</p>
					</div>
				</div>
				<p>@if($wantedCar)


				<div class="text-danger">
					<strong>{{ __('texts.is_car_wanted') }}: </strong> {{ __('texts.yes') }}
				</div>
				@else
					 <strong>{{ __('texts.is_car_wanted') }}: </strong> {{ __('texts.no') }}
			 	@endif
				</p>
				@if($wantedCar)
				<p class="text-danger">
					<strong>{{ __('texts.wanted_reason') }}:</strong> {{$wantedCar->reason}}
				</p>
				@endif @if(\App\WantedCar::where('plate',
				$car->plate)->where('is_wanted',
				true)->whereNotNull('image')->first())
				<p class="text-danger mouse-over"
					onclick="pictureOverlayOn('{{\App\WantedCar::where('plate', $car->plate)->where('is_wanted', true)->whereNotNull('image')->first()->image}}')">
					<strong>{{ __('texts.wanted_picture') }}: </strong> {{\App\WantedCar::where('plate',
					$car->plate)->where('is_wanted', true)->first()->image}}
				</p>
				@endif
				<p>
				@if($stolenCar)
					<div class="text-danger">
						<p>
							<strong>{{ __('texts.is_car_stolen') }}:</strong> {{ __('texts.yes') }}
						</p>
						<p>
							<strong>{{ __('texts.car_description') }}:</strong> {{$stolenCar->description}}
						</p>
					</div>
				@else
					<strong>{{ __('texts.is_car_stolen') }}:</strong> {{ __('texts.no') }}
				@endif
				</p>
				<div id="actions">
					@if( !Auth::guest() and (
					\Illuminate\Support\Facades\Auth::user()->hasRole('admin') or
					in_array(
					\Illuminate\Support\Facades\Auth::user()->getFiveMUserData()->job,
					\App\Http\Controllers\CarDealerController::ALLOWED_JOBS ) ) )
					<button class="btn btn-info" data-toggle="collapse"
						href="#plate-{{str_replace(' ', '', $car->plate)}}"
						aria-expanded="false"
						aria-controls="plate-{{str_replace(' ', '', $car->plate)}}">
						{{__('buttons.add_car_model_name')}}</button>
					<form method="post" onsubmit="overlayOn()" class="d-inline-block"
						action="{{route('carDealerUpdateInsurance')}}">
						@csrf <input type="hidden" value="{{$car->plate}}" name="plate">
						<button type="submit" class="btn btn-success">{{__('buttons.add_new_update_insurance')}}</button>
					</form>
					@endif
					@if( !Auth::guest() and (
					\Illuminate\Support\Facades\Auth::user()->hasRole('admin') or
					in_array(
					\Illuminate\Support\Facades\Auth::user()->getFiveMUserData()->job,
					\App\Http\Controllers\PoliceController::ALLOWED_JOBS ) ) ) {{--
					Police visible buttons --}} @if(\App\WantedCar::where('plate',
					$car->plate)->where('is_wanted', true)->first())
					<form action="{{route('policeUnDeclareCarAsWanted')}}"
						method="post" class="d-inline-block" onsubmit="overlayOn()">
						@csrf <input type="hidden" name="plate" value="{{$car->plate}}">
						<button type="submit" class="btn btn-warning">
							{{__('buttons.un_declare_car_as_wanted')}}</button>
					</form>
					@else
					<button class="btn btn-warning" data-toggle="collapse"
						href="#wanted-{{str_replace(' ', '', $car->plate)}}"
						aria-expanded="false"
						aria-controls="wanted-{{str_replace(' ', '', $car->plate)}}">
						{{__('buttons.declare_war_as_wanted')}}</button>
					@endif @if(\App\StolenCar::where('plate',
					$car->plate)->where('is_stolen', true)->first())
					<form class="d-inline-block" method="POST"
						action="{{route('policeUnDeclareCarAsStolen')}}"
						onsubmit="overlayOn()">
						@csrf <input type="hidden" name="plate" value="{{$car->plate}}">
						<button type="submit" class="btn btn-warning">
							{{__('buttons.un_declare_as_stolen')}}</button>
					</form>
					@else
					<button class="btn btn-warning" data-toggle="collapse"
						href="#stolen-{{str_replace(' ', '', $car->plate)}}"
						aria-expanded="false"
						aria-controls="stolen-{{str_replace(' ', '', $car->plate)}}">
						{{__('buttons.declare_car_as_stolen')}}</button>
					@endif
					<form method="POST" action="{{route('policeDeleteCar')}}"
						class="d-inline-block"
						onsubmit="return ConfirmDelete(); overlayOn();">
						@csrf <input type="hidden" name="carPlate" value="{{$car->plate}}">
						<input type="hidden" name="carOwner" value="{{$car->owner}}">
						<button type="submit" class="btn btn-danger">
							{{__('buttons.confiscate_car')}}</button>
					</form>


					@endif
					<div id="hidden-forms">

						{{--Car delear stuff--}}
						@if( !Auth::guest() and (
						\Illuminate\Support\Facades\Auth::user()->hasRole('admin') or
						in_array(
						\Illuminate\Support\Facades\Auth::user()->getFiveMUserData()->job,
						\App\Http\Controllers\CarDealerController::ALLOWED_JOBS ) ) )
						{{--Add car model name--}}
						<form id="plate-{{str_replace(' ', '',$car->plate)}}"
							method="POST" action="{{route('carDealerUpdateCarModelName')}}"
							class="collapse mt-3 mb-3" onsubmit="overlayOn()">
							@csrf <input type="hidden" name="carModelCode"
								value="{{json_decode($car->vehicle)->model}}"> <label
								for="carModelName">{{ __('forms.car_model_name') }}:</label> <input type="text"
								id="carModelName" name="carModelName" class="form-control"
								placeholder="{{ __('forms.car_model_name') }}" required>
							<button type="submit" class="btn btn-info mt-3"
								onclick="overlayOn()" id="confirmButtonChangeName" disabled>
								{{__('buttons.confirm_name_change')}}</button>
						</form>
						@endif

						{{-- Police stuff --}}
						@if( !Auth::guest() and (
						\Illuminate\Support\Facades\Auth::user()->hasRole('admin') or
						in_array(
						\Illuminate\Support\Facades\Auth::user()->getFiveMUserData()->job,
						\App\Http\Controllers\PoliceController::ALLOWED_JOBS ) ) ) {{--
						Wanted Car Form --}}
						<form id="wanted-{{str_replace(' ', '',$car->plate)}}"
							method="POST" action="{{route('policeDeclareCarAsWanted')}}"
							class="collapse mt-3 mb-3" enctype="multipart/form-data"
							onsubmit="overlayOn()">
							@csrf <input type="hidden" name="plate" value="{{$car->plate}}">
							<label for="wantedReason">{{ __('forms.reason') }}:</label>
							<textarea id="wantedReason" name="reason" class="form-control"
								placeholder="{{ __('forms.reason') }}" required></textarea>
							<label for="carImage" class="mt-2">{{ __('forms.picture') }}:</label> <input
								type="file" name="image" id="carImage"
								class="d-block btn-dark btn">
							<button type="submit" class="btn btn-info mt-3"
								id="confirmButtonWanted" disabled>{{__('buttons.confirm')}}</button>
						</form>

						{{-- Stolen Car Form --}}
						<form id="stolen-{{str_replace(' ', '',$car->plate)}}"
							method="POST" action="{{route('policeDeclareCarAsStolen')}}"
							class="collapse mt-3 mb-3" onsubmit="overlayOn()">
							@csrf <input type="hidden" name="plate" value="{{$car->plate}}">
							<label for="stolenDescription">{{ __('forms.description') }}:</label>
							<textarea id="stolenDescription" name="description"
								class="form-control" placeholder="{{ __('forms.description') }}" required></textarea>
							<button type="submit" class="btn btn-info mt-3"
								id="confirmButtonStolen" disabled>{{__('buttons.confirm')}}</button>
						</form>
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@if(\App\WantedCar::where('plate', $car->plate)->where('is_wanted',
true)->whereNotNull('image')->first())
<div class="overlay"
	id="pictureOverlay{{\App\WantedCar::where('plate', $car->plate)->where('is_wanted', true)->whereNotNull('image')->first()->image}}"
	onclick="pictureOverlayOff('{{\App\WantedCar::where('plate', $car->plate)->where('is_wanted', true)->whereNotNull('image')->first()->image}}')">
	<img class="center"
		src="{{'/images/'. \App\WantedCar::where('plate', $car->plate)->where('is_wanted', true)->whereNotNull('image')->first()->image}}">
</div>
@endif @endsection()

<script>
            window.onload = function () {
                const carModelName = document.getElementById('carModelName');
                const confirmButton = document.getElementById('confirmButtonChangeName');
                const wantedReason = document.getElementById('wantedReason');
                const stolenDescription = document.getElementById('stolenDescription');
                const confirmButtonWanted = document.getElementById('confirmButtonWanted');
                const confirmButtonStolen = document.getElementById('confirmButtonStolen');

                carModelName.addEventListener('keyup', function (event) {
                    let isValidCarModelName = carModelName.checkValidity();

                    if (isValidCarModelName) {
                        confirmButton.disabled = false;
                    } else {
                        confirmButton.disabled = true;
                    }
                });

                wantedReason.addEventListener('keyup', function (event) {
                    let isValidWanterReason = wantedReason.checkValidity();

                    if (isValidWanterReason) {
                        confirmButtonWanted.disabled = false;
                    } else {
                        confirmButtonWanted.disabled = true;
                    }
                });

                stolenDescription.addEventListener('keyup', function (event) {
                    let  isValidStolenDescription = stolenDescription.checkValidity();

                    if (isValidStolenDescription) {
                        confirmButtonStolen.disabled = false;
                    } else {
                        confirmButtonStolen.disabled = true;
                    }
                })
            };

            /**
             *
             * @returns {boolean}
             * @constructor
             */
            function ConfirmDelete() {
                var x = confirm("Are you sure you want to continue?");
                if (x)
                    return true;
                else
                    return false;
            }
        </script>
