@extends('layouts.app')
@section('content')
    <div class="container">
        @component(
            'components.searchBar',
             [
                'routeName' => $searchRoute,
                'placeholder' => __('forms.search_car'),
                'autocompleteUrl' => (isset($autocompleteUrl)) ? $autocompleteUrl : 'carPlateSearchAutocomplete'
             ]
        )
        @endcomponent

        <div class="card">
            <table class="table">
                <thead class="thead-light">
                <tr>
                    <th scope="col">{{__('tables.owner_name')}}</th>
                    <th>{{__('tables.number_plate')}}</th>
                    <th>{{__('tables.model_name')}}</th>
                    <th>{{__('tables.is_insured')}}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($cars as $car)
                    @if(\App\WantedCar::where('plate', $car->plate)->where('is_wanted', true)->first())
                        <tr class="mouse-over alert-danger" onclick="
                            window.location='{{route($route, [$car->plate])}}';
                            overlayOn();
                            ">
                    @elseif (\App\StolenCar::where('plate', $car->plate)->where('is_stolen', true)->first())
                        <tr class="mouse-over alert-warning" onclick="
                            window.location='{{route($route, [$car->plate])}}';
                            overlayOn();
                            ">
                    @else
                        <tr class="mouse-over" onclick="
                            window.location='{{route($route, [$car->plate])}}';
                            overlayOn();
                            ">
                            @endif
                            <td>
                                {{\App\Helpers\UserHelper::getCharacterName(\App\User::find($car->owner))}}
                            </td>
                            <td>{{ $car->plate}}</td>
                            <td>
                                @isset(\App\CarModel::where('code', json_decode($car->vehicle)->model)->first()->name)
                                    {{\App\CarModel::where('code', json_decode($car->vehicle)->model)->first()->name}}
                                @endisset
                            </td>
                            <td>
                                @if(
                                    isset(\App\CarInsurance::where('plate', $car->plate)->first()->is_insured)
                                    and isset(\App\CarInsurance::where('plate', $car->plate)->first()->end_time)
                                    and \App\CarInsurance::where('plate', $car->plate)->first()->is_insured
                                    and new DateTime(\App\CarInsurance::where('plate', $car->plate)->first()->end_time) > new DateTime()
                                )
                                    <div class="text-success">Yes</div>
                                @else
                                    <div class="text-danger">No</div>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                </tbody>
            </table>

            @if(method_exists($cars, 'links'))
                <div class="container">
                    <div class="pagination justify-content-center p-4">
                        {{$cars->links()}}
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
