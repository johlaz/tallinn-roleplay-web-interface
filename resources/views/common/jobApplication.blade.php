@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header"><b>{{__('headers.application_data')}}</b></div>
                <div class="card-body">
                    <div class="border-bottom border-light">
                        <p><strong>Steam name:</strong> {{$user->name}}</p>
                        <p><strong>Character name:</strong> {{$user->firstname}} {{$user->lastname}}</p>
                        <p><strong>Discord name:</strong> {{$answers->discordName}}</p>
                        <p><strong>Current job:</strong> {{$user->job}}</p>
                    </div>
                    <h5 class="mt-3"><strong>Application Answers</strong></h5>
                    @foreach($answers as $key => $answer)
                        <p>
                            <strong>{{ __('jobApplications.' . $key) }}:</strong>
                        </p>
                        <p>
                            {{$answer}}
                        </p>
                    @endforeach

                    <form action="{{route($acceptRoute)}}" method="post" class="d-inline-block">
                        @csrf
                        <input type="hidden" value="{{$application->id}}" name="id">
                        <input type="submit" class="btn btn-success" onclick="overlayOn()"
                               value="@if(isset($acceptButtonName)) {{$acceptButtonName}} @else {{__('buttons.accept')}} @endif">
                    </form>

                    <form action="{{route($rejectRoute)}}" method="post" class="d-inline-block">
                        @csrf
                        <input type="hidden" value="{{$application->id}}" name="id">
                        <input type="submit" class="btn btn-danger" value="{{__('buttons.reject')}}" onclick="overlayOn()">
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
