@extends('layouts.app')

@section('content')
    <div class="container">
        <h2 class="text-gray">
            {{__('headers.fill_criminal_record_for')}}
            {{\App\Helpers\UserHelper::getCharacterName(\App\User::find($patientIdentifier))}}
        </h2>
        <div class="card">
        	<div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form method="POST" action="{{route('addMedicalIncident')}}">
                    @csrf
                    <input type="hidden" name="patientId" value="{{$patientIdentifier}}">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="summary">{{__('forms.summary')}}</label>
                            <textarea class="form-control" id="summary" name="summary" rows="4" required></textarea>
                        </div>
                    </div>
                    <button class="btn btn-info" id="confirmButton" onclick="overlayOn()" disabled>{{__('buttons.create_record')}}</button>
                </form>
            </div>
        </div>
    </div>
@endsection
<script>
    window.onload = function () {
        const summaryField = document.getElementById('summary');
        const confirmButton = document.getElementById('confirmButton');

        summaryField.addEventListener('keyup', function (event) {
            let isValidSummary = summaryField.checkValidity();

            if (isValidSummary) {
                confirmButton.disabled = false;
            } else {
                confirmButton.disabled = true;
            }
        });
    }
</script>
