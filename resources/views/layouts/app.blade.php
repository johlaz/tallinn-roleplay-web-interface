<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/overlay.js') }}" defer></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js" defer></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js" defer></script>
    {{--Font awesome - icons --}}
    <script src="https://kit.fontawesome.com/39b27473ae.js"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/spinner.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    <!-- Discord Scripts -->
    <link href="{{ asset('css/discordInvite.css') }}" rel="stylesheet">
    <script src="{{ asset('js/discordInvite.js') }}"></script>
    <script src="{{ asset('js/overlay.js') }}"></script>
    <script>
        discordInvite.init({
            inviteCode: 'xuAv32Y',
            title: 'Tallinn Roleplay',
        });
        discordInvite.render();
    </script>
    <style>
        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }
    </style>
</head>
<body class="gradiant-background">
<div id="app">
    <nav class="navbar navbar-expand-md navbar-darkish navbar-laravel navbar-dark above-overlay shadow-lg p-3 mb-2">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                {{-- config('app.name', 'Laravel') --}}
                <img src="{{\Illuminate\Support\Facades\Storage::disk('local')->url('logo/tallinnrp2-w.png')}}">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{__('link.media')}} <span class="caret"></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="https://tallinnroleplay.ee/raadio/" onclick="overlayOn()">
                                {{__('link.radio')}}
                            </a>
                            <a class="dropdown-item" href="https://seitsmesed.wordpress.com/" onclick="overlayOn()">
                                {{__('link.news')}}
                            </a>
                        </div>
                    </li>
					<li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{__('link.help')}} <span class="caret"></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{route('help')}}" onclick="overlayOn()">
                                {{__('link.server_rules')}}
                            </a>
                            <a class="dropdown-item" href="{{route('info')}}" onclick="overlayOn()">
                                {{__('link.server_info')}}
                            </a>
                            <a class="dropdown-item" href="{{route('policeRules')}}" onclick="overlayOn()">
                                {{__('link.police_rules')}}
                            </a>
                            <a class="dropdown-item" href="{{route('emsRules')}}" onclick="overlayOn()">
                                {{__('link.ems_rules')}}
                            </a>
                            <a class="dropdown-item" href="{{route('showFinesInRules')}}" onclick="overlayOn()">
                                {{__('link.fines_list')}}
                            </a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{__('link.job_applications')}} <span class="caret"></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('jobApplicationPolice') }}" onclick="overlayOn()">
                                {{__('link.police_application')}}
                            </a>
                            <a class="dropdown-item" href="{{route('jobApplicationAmbulance')}}" onclick="overlayOn()">
                                {{__('link.ambulance_application')}}
                            </a>
                        </div>
                    </li>
                    @if (!Auth::guest() and \App\Helpers\Sql\WebUserHelper::checkWebUserConnectionWithFiveMUser(Auth::user()->id))
                        @if(!Auth::guest() and (
                            \Illuminate\Support\Facades\Auth::user()->hasRole('admin') or
                            in_array(
                                \Illuminate\Support\Facades\Auth::user()->getFiveMUserData()->job,
                                 \App\Http\Controllers\PoliceController::ALLOWED_JOBS
                             )
                        ))
                            {{-- Police menu--}}
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{__('link.police')}} <span class="caret"></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('policeAllUsers') }}" onclick="overlayOn()">
                                        {{__('link.players_list')}}
                                    </a>
                                    <a class="dropdown-item" href="{{route('policeShowAllCars')}}" onclick="overlayOn()">
                                        {{__('link.cars_list')}}
                                    </a>
                                    <a class="dropdown-item" href="{{route('policeShowAllReports')}}"
                                       onclick="overlayOn()">
                                        {{__('link.reports_list')}}
                                    </a>
                                    <a class="dropdown-item" href="{{route('showFines')}}" onclick="overlayOn()">
                                        {{__('link.fines_list')}}
                                    </a>
                                    <a class="dropdown-item" href="{{route('showPoliceOfficersList')}}"
                                       onclick="overlayOn()">
                                        {{__('link.officers_list')}}
                                    </a>
                                    <a class="dropdown-item" href="{{route('showAllPoliceBill')}}"
                                       onclick="overlayOn()">
                                        {{__('link.bills')}}
                                    </a>
                                    <a class="dropdown-item" href="{{route('policeShowApplicationsList')}}"
                                       onclick="overlayOn()">
                                        {{__('link.job_applications')}}
                                    </a>
                                    <a class="dropdown-item" href="{{route('policeShowAcceptedJobApplications')}}"
                                       onclick="overlayOn()">
                                        {{__('link.accepted_job_applications')}}
                                    </a>
                                </div>
                            </li>
                        @endif
                    @endif
                    {{-- ambulance menu--}}
                    @if (!Auth::guest() and \App\Helpers\Sql\WebUserHelper::checkWebUserConnectionWithFiveMUser(Auth::user()->id))
                        @if(!Auth::guest() and (
                            \Illuminate\Support\Facades\Auth::user()->hasRole('admin') or
                            in_array(
                                \Illuminate\Support\Facades\Auth::user()->getFiveMUserData()->job,
                                 \App\Http\Controllers\AmbulanceController::ALLOWED_JOBS
                             )
                        ))
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{__('link.ambulance')}}<span class="caret"></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('ambulanceAllUsers') }}"
                                       onclick="overlayOn()">
                                        {{__('link.players_list')}}
                                    </a>
                                    <a class="dropdown-item" href="{{ route('showEmsWorkersList') }}"
                                       onclick="overlayOn()">
                                        {{__('link.workers_list')}}
                                    </a>
                                    <a class="dropdown-item" href="{{ route('showAllEmsBill') }}"
                                       onclick="overlayOn()">
                                        {{__('link.bills')}}
                                    </a>
                                    <a class="dropdown-item" href="{{ route('emsShowApplicationsList') }}"
                                       onclick="overlayOn()">
                                        {{__('link.job_applications')}}
                                    </a>
                                    <a class="dropdown-item" href="{{route('emsShowAcceptedJobApplications')}}"
                                       onclick="overlayOn()">
                                        {{__('link.accepted_job_applications')}}
                                    </a>
                                </div>
                            </li>
                        @endif
                    @endif
                    {{-- Car dealer menu --}}
                    @if (!Auth::guest() and\App\Helpers\Sql\WebUserHelper::checkWebUserConnectionWithFiveMUser(Auth::user()->id))
                        @if(!Auth::guest() and (
                            \Illuminate\Support\Facades\Auth::user()->hasRole('admin') or
                            in_array(
                                \Illuminate\Support\Facades\Auth::user()->getFiveMUserData()->job,
                                 \App\Http\Controllers\CarDealerController::ALLOWED_JOBS
                             )
                        ))
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{__('link.car_dealer')}}<span class="caret"></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('usedCarsSellRequests') }}"
                                       onclick="overlayOn()">
                                        {{__('link.used_cars_sell_request_list')}}
                                    </a>
                                    <a class="dropdown-item" href="{{ route('usedCarsSellHistory') }}"
                                       onclick="overlayOn()">
                                        {{__('link.used_cars_sell_history')}}
                                    </a>
                                    <a class="dropdown-item" href="{{ route('showAllVehicles') }}"
                                       onclick="overlayOn()">
                                        {{__('link.all_available_vehicles_marks')}}
                                    </a>
                                    <a class="dropdown-item" href="{{ route('showAllCars') }}"
                                       onclick="overlayOn()">
                                        {{__('link.all_owned_cars')}}
                                    </a>
                                </div>
                            </li>
                        @endif
                    @endif
                    {{-- Mechanic menu --}}
                    @if (!Auth::guest() and\App\Helpers\Sql\WebUserHelper::checkWebUserConnectionWithFiveMUser(Auth::user()->id))
                        @if(!Auth::guest() and (
                            \Illuminate\Support\Facades\Auth::user()->hasRole('admin') or
                            in_array(
                                \Illuminate\Support\Facades\Auth::user()->getFiveMUserData()->job,
                                 \App\Http\Controllers\MechanicController::ALLOWED_JOBS
                             )
                        ))
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{__('link.mechanic')}}<span class="caret"></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('mechanicNewReport') }}"
                                       onclick="overlayOn()">
                                        {{__('link.new_report')}}
                                    </a>
                                    <a class="dropdown-item" href="{{ route('mechanicMyReports') }}"
                                       onclick="overlayOn()">
                                        {{__('link.my_reports')}}
                                    </a>
                                    <a class="dropdown-item" href="{{ route('mechanicAllReports') }}"
                                       onclick="overlayOn()">
                                        {{__('link.all_mechanic_reports')}}
                                    </a>
                                    <a class="dropdown-item" href="{{ route('showAllMechanicBill') }}"
                                       onclick="overlayOn()">
                                        {{__('link.bills')}}
                                    </a>
                                </div>
                            </li>
                        @endif
                    @endif
                    @if(!Auth::guest() && \Illuminate\Support\Facades\Auth::user()->hasAnyRole(['admin', 'mod']))
                        {{-- Admin menu --}}
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{__('link.admin')}}<span class="caret"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('adminAllUsers') }}" onclick="overlayOn()">
                                    {{__('link.all_users')}}
                                </a>
                                <a class="dropdown-item" href="{{ route('adminAllWhitelistApplications') }}"
                                   onclick="overlayOn()">
                                    {{__('link.whitelist_applications')}}
                                </a>
                                <a class="dropdown-item" href="{{ route('adminCkCandidates') }}"
                                   onclick="overlayOn()">
                                    {{__('link.ck_candidates')}}
                                </a>
                            </div>
                        </li>
                    @endif
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}" onclick="overlayOn()">{{__('link.login')}}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('whitelistApplication') }}" onclick="overlayOn()">
                                {{__('link.whitelist_application')}}
                            </a>
                        </li>
                    @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right text-center" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ url('/home') }}" role="button" onclick="overlayOn()">{{__('link.home')}}</a>
                                <a class="dropdown-item" href="#" onclick="showLegend()">{{ __('link.legend') }}</a>
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit(); overlayOn();">
                                    {{__('link.logout')}}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>

    <main class="py-4">
        @yield('content')
    </main>
</div>
@component('components.spinner')
@endcomponent
@component('common.legend')
@endcomponent
</body>
<script>
    document.onkeypress = processKey;

    function processKey(e)
    {
        if (null == e)
            e = window.event ;
        if (e.keyCode == 124)  {
            document.getElementById("legendOverlay").style.display = "block";
        }
    }
</script>
</html>
