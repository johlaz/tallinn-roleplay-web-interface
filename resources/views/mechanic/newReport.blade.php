@extends('mechanic.layout.layout')
@section('mechanicBody')
    <div class="card">
        <div class="card-header justify-content-center"><b>{{__('headers.mechanic_new_report')}}</b></div>

        <div class="card-body">
            <form method="POST" action="{{ route('mechanicSaveNewReport') }}">
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="jobType">{{ __('forms.mechanic_job_type') }}</label>
                        <select type="select" class="form-control" id="jobType" placeholder="{{__('forms.mechanig_job_type')}}"
                            name="jobType" value="{{ old('jobType') }}" required autofocus>
                            @foreach ($jobType as $value => $translation)
                                <option value = "{{$value}}">{{ __('forms.' . $translation) }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="jobPrice">{{ __('forms.mechanic_job_price') }}</label>
                        <input type="number" class="form-control" id="jobPrice" placeholder="{{__('forms.mechanic_job_price')}}"
                        name="jobPrice" value="{{ old('jobPrice') }}" required autofocus />
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="carPlate">{{ __('forms.car_plate') }}</label>
                        <input type="input" class="form-control" id="carPlate" placeholder="{{__('forms.car_plate')}}"
                        name="carPlate" value="{{ old('carPlate') }}" />
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <p>{{ __('texts.mechanic_service_type') }}</p>
                        @foreach ($serviceType as $value => $translation)
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="serviceType"
                                       id="{{ $value }}" value="{{ $value }}" @if ($value === "npc") checked @endif>
                                <label class="form-check-label" for="{{ $value }}">
                                    {{ __('forms.' . $translation) }}
                                </label>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="summary">{{__('forms.summary')}}</label>
                        <textarea class="form-control" id="summary" name="summary" rows="4"></textarea>
                    </div>
                </div>
                <div class="form-row">
                    <button class="btn btn-info" id="confirmButton" onclick="overlayOn()" disabled>{{__('buttons.create_record')}}</button>
                </div>
            </form>
        </div>
    </div>
@endsection

<script>
window.onload = function () {
    const jobPrice = document.getElementById('jobPrice');
    const confirmButton = document.getElementById('confirmButton');

    jobPrice.addEventListener('keyup', function (event) {
        let isValidJobPrice = jobPrice.checkValidity();

        if (isValidJobPrice) {
            confirmButton.disabled = false;
        } else {
            confirmButton.disabled = true;
        }
    });
}
</script>
