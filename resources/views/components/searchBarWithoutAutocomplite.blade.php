<div class="pb-5">
    <form class="form-inline" action="{{route($routeName)}}" method="get">
        <input type="text" class="form-control col-md-9 typeahead" id="search" name="search" placeholder="{{$placeholder}}"
               @isset($oldSearch) value="{{$oldSearch}}" @endisset autocomplete="off">
        <button type="submit" class="btn btn-primary col-md-2 offset-md-1" onclick="overlayOn()">
            {{__('buttons.search')}}
        </button>
    </form>
</div>
