@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                @if(null !== session('carSold'))
                    @if(!session('carSold'))
                        <div class="alert alert-danger" role="alert">
                            Car sell confirmation failed
                        </div>
                    @endif
                @endif
                <div class="card">
                    <div class="card-header"><b>{{__('headers.car_sale_confirmation')}}</b></div>
                    <div class="card-body">
                        <h5 class="mb-5">Please check seller ID Card before confirming sell</h5>
                        <p><b>Seller Steam Name: </b>{{$sellerInfo->name}}</p>
                        <p><b>Seller Character Name: </b>{{\App\Helpers\UserHelper::getCharacterName($sellerInfo)}}</p>

                        <div class="border-bottom mb-3"></div>
                        <p><b>Seller Steam Name: </b>{{$buyerInfo->name}}</p>
                        <p><b>Seller Character Name: </b>{{\App\Helpers\UserHelper::getCharacterName($buyerInfo)}}</p>

                        <div class="border-bottom mb-3"></div>
                        <p><b>Car number plate: </b>{{$sellRequest->plate}}</p>
                        <div class="border-bottom mb-3"></div>
                        @if($sellRequest->confirmedBy)
                            <p><b>Confirmed by:</b> {{$sellRequest->confirmedBy}}</p>
                        @else
                            <form method="POST" action="{{route('finalizeUsedCarSale')}}">
                                @csrf
                                <input type="hidden" name="sellRequestId" value="{{$sellRequest->id}}">
                                <div class="form-check mb-3">
                                    <input class="form-check-input" type="checkbox" id="nameChecked" name="nameChecked" required>
                                    <label class="form-check-label" for="nameChecked">
                                        I confirm that Seller identity has been verified
                                    </label>
                                </div>
                                <button type="submit" class="btn btn-info" onclick="overlayOn()">{{__('buttons.confirm_sell')}}</button>
                            </form>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<script>

    /**
     *
     * @returns {boolean}
     * @constructor
     */
    function ConfirmDelete()
    {
        var x = confirm("Are you sure you want to continue?");
        if (x)
            return true;
        else
            return false;
    }

</script>
