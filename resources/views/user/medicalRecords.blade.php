@extends('user.layout.layout')

@section('userBody')
    <div class="card mt-5">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        @if (session('error'))
            <div class="alert alert-danger" role="alert">
                {{ session('error') }}
            </div>
        @endif
        @component('components.medicalHistoryTable', ['characters' => $characters, 'identifier' => $steamId, 'medicalHistory' => $medicalHistory, 'actions' => false])
        @endcomponent
    </div>
@endsection
<script>

    /**
     *
     * @returns {boolean}
     * @constructor
     */
    function ConfirmSell() {
        var x = confirm("Are you sure you want to continue?");
        if (x)
            return true;
        else
            return false;
    }

</script>
