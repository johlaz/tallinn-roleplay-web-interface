<ul class="nav nav-tabs nav-tabs-dark">
    <li class="nav-item">
        <a @if(\Illuminate\Support\Facades\Route::currentRouteName() === 'home') class="nav-link-dark active-dark" @else class="nav-link-dark" @endif href="{{route('home')}}" onclick="overlayOn()">{{__('link.my_data')}}</a>
    </li>
    <li class="nav-item">
        <a @if(\Illuminate\Support\Facades\Route::currentRouteName() === 'myCars') class="nav-link-dark active-dark" @else class="nav-link-dark" @endif href="{{route('myCars')}}" onclick="overlayOn()">{{__('link.my_car')}}</a>
    </li>
    <li class="nav-item">
        <a @if(\Illuminate\Support\Facades\Route::currentRouteName() === 'myCriminalRecords') class="nav-link-dark active-dark" @else class="nav-link-dark" @endif href="{{route('myCriminalRecords')}}" onclick="overlayOn()">{{__('link.criminal_records')}}</a>
    </li>
    <li class="nav-item">
        <a @if(\Illuminate\Support\Facades\Route::currentRouteName() === 'myMedicalRecords') class="nav-link-dark active-dark" @else class="nav-link-dark" @endif href="{{route('myMedicalRecords')}}" onclick="overlayOn()">{{__('link.medical_records')}}</a>
    </li>
    <li class="nav-item">
        <a @if(\Illuminate\Support\Facades\Route::currentRouteName() === 'showJobApplications') class="nav-link-dark active-dark" @else class="nav-link-dark" @endif href="{{route('showJobApplications')}}" onclick="overlayOn()">{{__('link.my_Job_applications')}}</a>
    </li>
</ul>
