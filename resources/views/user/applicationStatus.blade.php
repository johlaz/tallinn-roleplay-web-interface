@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <h2 class="text-gray">{{__('texts.welcome')}}  {{ Auth::user()->name }}</h2>

                <div class="card">
                    <div class="card-header"><b>{{ __('headers.whitelist_application_info') }}</b></div>
                    <div class="card-body">
                        @if(!isset($whitelistApplication) or !$whitelistApplication)
                            <span class="text-danger">
                                <p>
                                    {{ __('texts.you_dont_have_whitelist_application_and_you_dont_have_character') }}
                                </p>
                                <p>
                                    {{ __('texts.contact_admins') }}
                                </p>
                            </span>
                        @else
                            <p><strong>{{__('texts.character_name')}}:</strong> {{$whitelistApplication->characterName}}</p>
                            <p><strong>{{__('texts.steam_name')}}:</strong> {{$whitelistApplication->steamName}}</p>
                            <p><strong>{{ __('texts.steam_hex') }}:</strong> {{substr($whitelistApplication->identifier, 6)}}</p>
                            {{ __('texts.your_whitelist_application') }}
                            @if($whitelistApplication->isAccepted)
                                {{ __('texts.has') }} <span class="badge-success pl-2 pr-2"><strong class="text-uppercase">{{ __('texts.accepted') }}</strong></span>
                                <p class="mt-3">{{ __('texts.login_to_fivem_server_create_you_character_and_then_come_back_and_push_continue') }}</p>
                                <a href="{{route('home')}}" class="btn btn-success" onclick="overlayOn()">{{ __('buttons.continue') }}</a>
                            @endif
                            @if($whitelistApplication->isRejected)
                                {{ __('texts.has') }} <span class="badge-danger pl-2 pr-2"><strong
                                        class="text-uppercase">{{ __('texts.rejected') }}</strong></span>
                                <p>
                                    {{ __('texts.reason') }}: {{$whitelistApplication->rejectionReason}}
                                </p>
                                <a href="{{route('whitelistApplicationUpdateView')}}" class="btn btn-info" onclick="overlayOn()">{{ __('buttons.update_whitelist_application') }}</a>
                            @endif
                            @if(!$whitelistApplication->isRejected and !$whitelistApplication->isAccepted)
                                {{ __('texts.is') }} <span class="badge-secondary pl-2 pr-2"><strong
                                        class="text-uppercase">{{ __('texts.pending') }}</strong></span>
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
