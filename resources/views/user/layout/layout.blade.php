@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <h2 class="text-gray">
                    {{__('texts.welcome')}}
                    {{ \App\Helpers\UserHelper::getCharacterName( Auth::user()->getFiveMUserData()) }}
                </h2>
                @component('user.components.secondaryNav')
                @endcomponent

                @if (session('status'))
                    <div class="alert alert-success mt-5" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                @if (session('error'))
                    <div class="alert alert-danger mt-5" role="alert">
                        {{ session('error') }}
                    </div>
                @endif

                @yield('userBody')
            </div>
        </div>
    </div>
@endsection
