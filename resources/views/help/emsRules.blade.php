@extends('help.layout.layout')
@section('helpHeader')
    <strong>Kiirabi Reeglid</strong>
@endsection
@section('helpBody')
    <div>
        <h4 id="Üldreeglid"><strong>Üldreeglid</strong></h4>
        <ol>
            <li>Kõik alluvad endast kõrgemale astmele</li>
            <li>Oled eeskujulik liikleja</li>
            <li>ERARIIETES töö tegemine + niisama on-dudy istumine keelatud!</li>
            <li>Inimese aitamine on number 1. Sellel hetkel oma asjadega ei tegele</li>
            <li>Inimese tahtlik tapmine = vallandamine</li>
            <li>Kedagi heast peast peksma ei hakka</li>
            <li>Suuremal kokkupõrkel niisama edasi ei sõida vaid kutsub abi</li>
            <li>Kui keegi sind tapab siis sa ei maksa talle ise kätte vaid kirjutad politseis avalduse või annad ülemusele teada</li>
            <li>Ülemus võib teha sulle noomituse ja sa pead sellest kinni pidama</li>
            <li>Ülemus võib töötajale teha rahatrahvi kui ta asjadest kinni ei pea</li>
            <li>Vilkurite põhjendamatu sisse lülitamine KEELATUD!</li>
        </ol>

        <h4 id="Rööv"><strong>Politsei-Kiirabi koostöö leping</strong></h4>
        <ol>
            <li>Röövidele peab minema kohale vähemalt 1 kiirabi</li>
            <li>Politsei kutseid tuleb teenindada eelisjärjekorras. Sama teeb ka politsei kiirabi sutes</li>
            <li>Politsei võib kutsuda meediku endaga kaasa ka teist tüüpi väljakutsetele kui röövid</li>
            <li>Politsei TULD ilma asjata ei ava! (Ainult juhul kui pätt on sinu vastu tule avanud ning olukord seda soodustab)</li>
            <li>Kiirabi võib vajadusel politsei kutsuda endaga kaasa väljakutsele kui tunneb ohtu</li>
            <li> Politsei korraldusi tuleb täita v.a. juhul kui need on vastuolus seadusega</li>
        </ol>
    </div>
@endsection
