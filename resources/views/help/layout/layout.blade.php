@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @component('help.components.secondaryNav')
                @endcomponent
                <div class="card mt-5">
                    <div class="card-header">
                        @yield('helpHeader')
                    </div>
                    <div class="card-body">
                        @yield('helpBody')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endSection
