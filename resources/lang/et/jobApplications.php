<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Registration Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'age' => 'Vanus (IRL)',
    'false' => 'Ei',
    'true' => 'Jah',
    'discordName' => 'Discordi nimi',
    'chitChat' => 'Kus sa lobised teiste kiirabi töötajatega?',
    'situation1' => 'Kirjelda, mida sa teed järgnevas olukorras. Sa saabud sündmuskohale, kus ohver lebab keset teed'
        . ' Mis sa teed oma autoga? Mida sa teed ohvriga? Kuidas sa lahendad olukorra?',
    'futurePlans' => 'Mis on su tuleviku plaanid?',
    'havePlayedRp' => 'Kaua sa oled roleplay-id mänginud?',
    'playingTimes' => 'Kui tihti sa mängid TallinnRP?',
    'irlExperienceEms' => 'Kas sul on päriselus kogemusi kiirabi tööga?',
    'sirensAllowed' => 'Kas kiirabil on lubatud sõita vilkuritega, kui ei ole väljakutset?',
    'criminalActions' => 'Kas kiirabi töötajal on lubatud tegeleda kriminaalse tegevusega?',
    'emsRpExperience' => 'Kas sul on eelnevat kogemust kiirabi tööga RP-s?',
    'numberOfJobGrades' => 'Mitu (au)astet on kiirabis?',
    'whyYouWantToBeEms' => 'Miks sa tahad saada kiirabiks?',
    'characterDescription' => 'Lühike kirjeldus oma karakterist',
    'question1' => 'Küsimus 1: Sa ajad kurjategijaid taga autoga. Kurjategija peatab auto, väljub autost ja hakkab '
        . 'jooksma metsa poole. Mis sa teed?',
    'question2' => 'Küsumus 2: Kaks meest võitlevad avalikus kohas. Järgmine hetk võtab üks isik noa välja. '
        . 'Kuidas sa käitud?',
    'microphone' => 'Kas sul on mikrofon?',
    'trafficStop' => 'Sa peatad auto kinni. Kirjelda, mida sa küsid, kuidas käitud, mida sa kontrollid, jne.',
    'otherServers' => 'Nimeta teised serverid, kus mängid ja millises oled kõige aktiivsem?',
    'irlExperiencePolice' => 'Kas sul on päriselus kogemust politsei tööga?',
    'whenYouUseGun' => 'Kirjelda vähamelt kaht olukorda, kus sa võid kasutada tulirelva',
    'copInAnotherServer' => 'Kas sa oled aktiivne politsei mõnes teises serveris',
    'previousExperience' => 'Kas sul on eelnevat kogemust politsei RP-ga? Kui jah, nimeta server(id)',
    'whyYouWantToBePolice' => 'Miks sa tahad saada politseiks?',
    'every_day' => 'Igapäev',
    'every_day_after_work_school' => 'Igapäev peale kooli/tööd',
    'only_weekends' => 'Ainult nädalavahetustel',
    'cant_answer_depends_on_how_much_time_I_have_after_work_and_personal_life' => 'Ei oska täpselt öelda, oleneb kuidas aega on',
    'other_servers' => 'Teised serverid',
    'previous_experience' => 'Eelnevad kogemused',
    'describe_your_actions' => 'Kirjelda oma tegevust',
    'will_shoot_criminal_with_my_firearm' => 'Tulistan isikut tulirelvast',
    'will_hit_criminal_with_a_car' => 'Sõidan isikule autoga otsa',
    'will_drive_as_close_as_possible_exit_a_car_and_start_chase_criminal_on_foot_If_possible_i_will_use_taser_to_stop_criminal' => 'Sõidan nii lähedale, kui võimalik, väljun autost ja asun isikut jalgsi taga ajama. Kui võimalik, siis kasutan tema peal tezerit',
    'will_run_next_to_the_criminal_and_cuff_him_her_while_he_she_is_still_running' => 'Jooksen isiku kõrvale ja panen ta raudu',
    'will_stop_chasing_and_i_will_go_back_to_patrol' => 'Lõpetan taga ajamise ja naasen patrulli',
    'will_shoot_him_because_why_not' => 'Tulistan teda, sest miks mitte',
    'will_take_out_a_firearm_and_aim_it_to_man_who_holds_a_knife_i_will_demand_him_to_put_down_a_knife_and_raise_his_hands_up' => 'Võtan välja tulirelva, sihin isikut, kes nuga käes hoiab ning nõuan, et ta noa käest paneks ja tõstaks käed ülesse',
    'will_run_back_to_my_car_and_hit_both_man_with_my_car' => 'Jooksen enda autosse ja ajan mõlemead isikud alla',
    'will_run_away_because_i_am_scared_of_my_life' => 'Jooksen minema, kuna ma kardan oma elu pärast',
    'will_watch_a_fight_because_it_is_fun' => 'Vaatan lõbu lihtsalt pealt',
    'in_discord' => 'Discrodis',
    'in_game' => 'Mängus',
];
