<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Buttons
    |--------------------------------------------------------------------------
    |
    | This file contains translations of buttons
    |
    */

    'search' => 'Otsi',
    'sell_car' => 'Müü Auto',
    'confirm_sell' => 'Kinnita Müük',
    'cancel_sell_request' => 'Tühista Auto Müük',
    'car_is_not_stolen_any_more' => 'Auto Ei Ole Enam Varastatud',
    'back_to_previous_page' => 'Tagasi Eelnevale Lehele',
    'delete' => 'Kustuta',
    'delete_user' => 'Kustuta Kasutaja',
    'remove' => 'Eemalda',
    'edit' => 'Muuda',
    'open' => 'Ava',
    'edit_record' => 'Muuda Kirje',
    'create_new_record' => 'Loo Uus Kirje',
    'create_record' => 'Loo Kirje',
    'remove_tattoos' => 'Eemaldage Tätoveeringud',
    'add_new_fines' => 'Lisage Uued Trahvid',
    'edit_fine' => 'Muuda Trahvi',
    'add_fine' => 'Lisa Uus Trahv',
    'accept' => 'Nõustu',
    'reject' => 'Keeldu',
    'hire' => 'Palka',
    'add_car_model_name' => 'Lisa Auto Mudeli Nimi',
    'add_new_update_insurance' => 'Lisa Uus / Värskenda Kindlustust',
    'un_declare_car_as_wanted' => 'Tühista Auto Tagaotsimiskuulutus',
    'declare_war_as_wanted' => 'Kuuluta Auto Tagaotsitavaks',
    'un_declare_as_stolen' => 'Tühista Auto Varguse avaldus',
    'declare_car_as_stolen' => 'Kuuluta Auto Varastatuks',
    'confiscate_car' => 'Konfiskeeri Auto',
    'confirm_name_change' => 'Kinnita Nime Muutus',
    'confirm' => 'Kinnita',
    'un_ban_user_from_whitelist' => 'Tühista Kasutaja Whitelist-i Ban',
    'ban_user_from_whitelist' => 'Ban-i Kasutaja Whitelist-ist',
    'un_declare_as_fugitive' => 'Tühista Tagaotsimise Kuulutus',
    'declare_as_fugitive' => 'Kuuluta Tagaotsitavaks',
    'login' => 'Logi Sisse',
    'send_password_reset_link' => 'Saada Parooli Lähtestamise Link',
    'reset_password' => 'Lähtesta Parool',
    'register' => 'Registreeri',
    'submit_application' => 'Esita Taotlus',
    'continue' => 'Jätka',
    'update_whitelist_application' => 'Uuenda Whitlisti Avaldust',
    'set_call_sign' => 'Määra Kutsung',
    'get_archive' => 'Vaata arhiivi',
];
