<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Buttons
    |--------------------------------------------------------------------------
    |
    | This file contains translations of buttons
    |
    */
    'e_mail_address' => 'E-Mail Address',
    'password' => 'Password',
    'confirm_password' => 'Confirm Password',
    'remember_me' => 'Remember Me',
    'buyer_name' => 'Buyer Name',
    'name' => 'Nimi',
    'character_name' => 'Character Name',
    'steam_name' => 'Steam Name',
    'steam_hex' => 'Steam Hex',
    'jail_time' => 'Jail Time',
    'make_warning_about_car' => 'Warning about car',
    'make_warning_about_traffic_violation_drives_license' => 'Warning about traffic violation/drives license',
    'make_warning_about_gun_law_violation_gun_licence' => 'Warning about gun law violation/gun Licence',
    'it_s_criminal_case' => 'It\'s criminal case',
    'it_s_not_criminal_case' => 'It\'s not criminal case',
    'summary' => 'Summary',
    'profile_picture' => 'Profile Picture',
    'search_user' => 'Search user',
    'search_car' => 'Search cars by plate',
    'search_fine' => 'Search fines',
    'search_records' => 'Search record by id, criminal name or police officer name',
    'search_car_sell_request' => 'Search car sell request by plate or request number',
    'car_model_name' => 'Car Model Name',
    'reason' => 'Reason',
    'picture' => 'Picture',
    'description' => 'Description',
    'mechanic_job_type' => 'Job type',
    'mechanic_car_trasnport' => 'Car Transport',
    'mechanic_car_fix' => 'Car Fix',
    'mechanic_car_tuning' => 'Car Tuning',
    'mechanic_car_deals' => 'Car Deals',
    'mechanic_job_price' => 'Job Price',
    'mechainc_npc' => 'Local',
    'mechainc_citizen' => 'Citizen',
    'mechainc_ambulance' => 'Ambulance',
    'mechainc_police' => 'Police',
    'mechainc_call' => 'Police call',
    'car_plate' => 'Car Plate',
    'whenYouCanUseWeapons' => 'When Can you use weapons?',
    'newLifeRule' => 'What happens if you die and EMS can not review you?',
    'helmetMaskRules' => 'When is allowed to use mask, when it is allowed to use helmet?',
    'robbingOtherCitizen' => 'When is allowed to rob other citizen?',
    'carTax' => 'When you need to pay car tax?',
    'vdmRdm' => 'Explain what is RDM, VDM and cop bait?',
    'oocIc' => 'Explain what is OOC and IC?',
    'radioUsage' => 'Who are allowed to use radio (discord voice chats)?',
    'call_sign' => 'Call Sign',
    'place' => 'Place',
    'other_parties' => 'Other parties',
];
