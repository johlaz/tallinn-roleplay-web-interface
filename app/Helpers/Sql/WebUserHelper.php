<?php
namespace App\Helpers\Sql;

use Illuminate\Support\Facades\DB;

/**
 * Get web user data
 */
class WebUserHelper
{

    /**
     * Check if web page user is connected with FiveM user
     * @param $webUserId
     * @return bool
     * @throws \Exception
     */
    public static function checkWebUserConnectionWithFiveMUser($webUserId)
    {

        try {
            if(!DB::table('user_web_user')->where('web_user_id', '=', $webUserId)->first()) {
                return false;
            }

            return true;
        } catch (\Exception $exception) {
            throw new \Exception($exception->getMessage());
        }
    }
}
