<?php
namespace App\Helpers;

use App\Bill;
use App\User;

/**
* Helper to manipulate data from bills
*/
class BillsHelper
{

    /**
     * @param mixed $sociatyBills
     * @param int $allBillsCount
     * @return int|float
     */
    public static function getPrecentOf($sociatyBills, $allBillsCount)
    {

        if(!$allBillsCount or !$sociatyBills){
            return 0;
        }
        $percent = $sociatyBills->count() / $allBillsCount;
        return number_format( $percent * 100, 2 );
    }

    public static function getSocietyBills($society)
    {
        $bills = Bill::where('target', $society)->orderBy('identifier', 'DESC')->get();
        $billsCollection = [];

        $lastIdentifier = null;
        foreach ($bills as $bill) {
            if($lastIdentifier !== $bill->identifier) {
                $lastIdentifier = $bill->identifier;
                $user = User::where('identifier', $bill->identifier)->first();
                $billsCollection[$bill->identifier] = [
                    'steamName' => $user->name,
                    'characterName' => $user->firstname . ' ' . $user->lastname,
                    'amount' => [$bill->amount]
                ];
            } else {
                $identifier = $bill->identifier;
                $billsCollection[$identifier]['amount'][] = $bill->amount;
            }
        }

        //calculate persons total amount
        foreach ($billsCollection as $identifier => $personBills) {
            $total = 0;
            foreach ($personBills['amount'] as $amount) {
                $total = $total + $amount;
            }
            $billsCollection[$identifier]['total'] = $total;
        }

        $total = 0;
        foreach ($billsCollection as $personBills) {
            $total = $total + $personBills['total'];
        }

        $billsCollection = collect($billsCollection);

        return [
            'billsCollection' => $billsCollection->sortByDesc('total'),
            'total' => $total,
            'count' => $bills->count(),
        ];
    }
}
