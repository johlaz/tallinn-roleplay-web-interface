<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo as BelongsTo;

class MedicalHistory extends Model
{

    /**
     * @return BelongsTo
     */
    public function user()
    {

        return $this->belongsTo(User::class);
    }

    /**
     * @return BelongsTo
     */
    public function emsWorker()
    {

        return $this->belongsTo(EmsWorker::class);
    }
}
