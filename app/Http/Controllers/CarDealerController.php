<?php

namespace App\Http\Controllers;

use App\CarInsurance;
use App\CarModel;
use App\CarSellRequest;
use App\OwnedCar;
use App\StolenCar;
use App\User;
use App\Vehicle;
use App\WantedCar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CarDealerController extends Controller
{

    /**
     * @var array
     */
    const ALLOWED_JOBS = [
        'cardealer',
        'offcardealer',
        'police',
        'offpolice',
        'delux_cardealer',
        'offdelux_cardealer',
        'mechanic',
        'offmechanic'
    ];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function showUsedCarSellRequest()
    {

        $this->restrictAccess();

        return view(
            'carDealer.usedCars.lists.waitingConfirmation',
            [
                'sellRequests' => CarSellRequest::where('confirmedBy', null)->get(),
                'searchRouteName' => 'usedCarsSellRequestSearch'
            ]
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function searchCarSellRequest(Request $request)
    {

        $this->restrictAccess();
        $searchResults = CarSellRequest::where('id', $request->search)
            ->orWhere('plate', 'like', "%{$request->search}%")
            ->where('confirmedBy', null)
            ->get();

        return view(
            'carDealer.usedCars.lists.waitingConfirmation',
            [
                'sellRequests' => $searchResults,
                'searchRouteName' => 'usedCarsSellRequestSearch'
            ]
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function searchCarSellHistory(Request $request)
    {

        $this->restrictAccess();
        $searchResults = CarSellRequest::where('id', $request->search)
            ->orWhere('plate', 'like', "%{$request->search}%")
            ->orWhere('confirmedBy', 'like', "%{$request->search}%")
            ->wherenotnull('confirmedBy')
            ->get();

        return view(
            'carDealer.usedCars.lists.waitingConfirmation',
            [
                'sellRequests' => $searchResults,
                'history' => true,
                'searchRouteName' => 'sellRequestHistorySearch'
            ]
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function showCarSellRequest(Request $request)
    {

        $this->restrictAccess();
        $sellRequest = CarSellRequest::where('id', $request->sellRequestId)->first();

        return view(
            'carDealer.usedCars.sellRequest',
            [
                'sellRequest' => $sellRequest,
                'sellerInfo' => User::find($sellRequest->sellerId),
                'buyerInfo' => User::find($sellRequest->buyerId)
            ]
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function finalizeUsedCarSale(Request $request)
    {

        $this->restrictAccess();
        $sellRequest = CarSellRequest::find($request->sellRequestId);
        try {
            $loggedInUserData = Auth::user()->getFiveMUserData();
            $car = OwnedCar::where('plate', $sellRequest->plate)->first();
            $car->owner = $sellRequest->buyerId;
            $car->save();

            $sellRequest->confirmedBy = $loggedInUserData->name;
            $sellRequest->save();
            return redirect()->route('usedCarsSellRequests')->with('carSold', true);
        } catch (\Exception $exception) {
            throw new \Exception($exception->getMessage());
            return redirect()->back()->with('carSold', false);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function showUsedCarSellHistory()
    {

        $this->restrictAccess();

        return view(
            'carDealer.usedCars.lists.waitingConfirmation',
            [
                'sellRequests' => CarSellRequest::whereNotNull('confirmedBy')->get(),
                'history' => true,
                'searchRouteName' => 'sellRequestHistorySearch'
            ]
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function showAllVehicles()
    {

        $this->restrictAccess();

        return view(
            'carDealer.allVehicles',
            [
                'vehicles' => Vehicle::all()
            ]
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function showAllCars()
    {

        $this->restrictAccess();
        $cars = DB::table('owned_vehicles')
            ->paginate(10);

        return view(
            'common.allCars',
            [
                'cars' => $cars,
                'route' => 'carDealerShowSingleCar',
                'autocompleteUrl' => 'carPlateSearchAutocomplete',
                'searchRoute' => 'carDealerSearchCar'
            ]
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function searchCar(Request $request)
    {

        $this->restrictAccess();;
        $cars = DB::table('owned_vehicles')
            ->whereRaw('LOWER(plate) like :plate', ['plate' => '%' . strtolower($request->input('search')) . '%'])
            ->get();

        return view(
            'common.allCars',
            [
                'cars' => $cars,
                'route' => 'carDealerShowSingleCar',
                'oldSearch' => $request->search,
                'autocompleteUrl' => 'carPlateSearchAutocomplete',
                'searchRoute' => 'carDealerSearchCar'
            ]
        );
    }

    /**
     * @param Request $request
     * @param $plate
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function showSingleCar($plate)
    {

        $this->restrictAccess();
        $car = OwnedCar::where('plate', $plate)->first();

        return view(
            'common.carData',
            [
                'car' => $car,
                'user' => User::find($car->owner),
                'wantedCar' => WantedCar::where('plate', $car->plate)->where('is_wanted', true)->first(),
                'stolenCar' => StolenCar::where('plate', $car->plate)->where('is_stolen', true)->first()
            ]
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function updateCarModelName(Request $request)
    {

        $this->restrictAccess();

        try {
            $carModel = CarModel::where('code', $request->carModelCode)->first();

            if ($carModel) {
                $carModel->name = $request->carModelName;
                $carModel->save();

                return redirect()->back()->with('status', 'Car model name updated');
            }

            $carModel = new CarModel();
            $carModel->name = $request->carModelName;
            $carModel->code = $request->carModelCode;
            $carModel->save();

            return redirect()->back()->with('status', 'Car model name added');
        } catch (\Exception $exception) {
            return redirect()->back()->with('error', 'Something went wrong: ' . $exception->getMessage());
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function updateInsurance(Request $request)
    {

        $this->restrictAccess();

        try{
            $insurance = CarInsurance::where('plate', $request->plate)->first();

            if ($insurance) {
                $insurance->is_insured = true;
                $insurance->end_time = (new \DateTime())->modify('+1 month');
                $insurance->save();

                return redirect()->back()->with('status', 'Insurance updated');
            }

            $insurance = new CarInsurance();
            $insurance->plate = $request->plate;
            $insurance->is_insured = true;
            $insurance->end_time = (new \DateTime())->modify('+1 month');
            $insurance->save();

            return redirect()->back()->with('status', 'Insurance Added');

        } catch (\Exception $exception) {

            return redirect()->back()->with('error', 'Something went wrong: ' . $exception->getMessage());
        }
    }

    /**
     * Control User job in Server and if not police or fib, then dont allow to continue
     */
    private function restrictAccess()
    {

        $loggedInUserData = Auth::user()->getFiveMUserData();

        $isAdmin = Auth::user()->hasRole('admin');
        if(!in_array($loggedInUserData->job, self::ALLOWED_JOBS) and !$isAdmin) {
            abort(401, 'This action is unauthorized.');
        }

        return;
    }
}
