<?php

namespace App\Http\Controllers;

use App\Bill;
use App\Character;
use App\EmsWorker;
use App\Helpers\BillsHelper;
use App\Helpers\Sql\FivemUserHelper;
use App\Helpers\UserHelper;
use App\Helpers\WantedHelper;
use App\JobApplication;
use App\JobGrades;
use App\MedicalHistory;
use App\User;
use App\WantedCharacters;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use function Psy\bin;

/**
 * Class AdminController
 */
class AmbulanceController extends Controller
{

    const ALLOWED_JOBS = [
        'ambulance',
        'offambulance'
    ];

    const ALLOWED_JOB_GRADES = [
        99
    ];

    const TATTOO_REMOVAL_ALLOWED_JOB_GRADES = [
        2,
        3,
        99
    ];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->middleware('auth');
    }

    /**
     * Show list of all fivem users
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function allUsers()
    {

        $this->restrictAccess();

        return view(
            'common.alluser',
            [
                'users' => User::paginate(10),
                'wanted' => WantedHelper::getAllWantedCharacters(),
                'searchRoute' => 'ambulanceSearchUser',
                'searchPlaceholder' => __('forms.search_user'),
                'singleUserRoute' => 'ambulanceSingleUser',
            ]
        );
    }

    /**
     * Show single fivem user info and actions
     *
     * @param string $steamId
     * @param bool $archive
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function singleUser($steamId, $archive = false)
    {

        $this->restrictAccess();

        $user = User::find($steamId);

        if ($archive) {
            $medicalHistory = $user->medicalHistory()->orderBy('id', 'DESC')->get();
        } else {
            $filterDate = (new Carbon())->modify('-1 month');
            $medicalHistory = $user->medicalHistory()->where('updated_at', '>=', $filterDate->format('Y-m-d h:m:s'))->orderBy('id', 'DESC')->get();
        }
        return view(
            'ambulance.user',
            [
                'user' => $user,
                'bills' => Bill::where('identifier', $steamId),
                'wanted' => $user->wantedCharacter()->get(),
                'characters' => Character::where('identifier', $steamId)->get(),
                'medicalHistory' => $medicalHistory,
                'profilePicture' => ($user->webUser()->first()->profilePicture()->orderBy('id', 'DESC')->first()) ? $user->webUser()->first()->profilePicture()->orderBy('id', 'DESC')->first()->path : null
            ]
        );
    }

    /**
     * Show search results of users
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function searchUser(Request $request)
    {

        $this->restrictAccess();
        return view(
            'common.alluser',
            [
                'users' => FivemUserHelper::searchUsers($request->input('search')),
                'wanted' => WantedHelper::getAllWantedCharacters(),
                'searchRoute' => 'ambulanceSearchUser',
                'searchPlaceholder' => 'search user',
                'singleUserRoute' => 'ambulanceSingleUser',
                'oldSearch' => $request->search
            ]
        );
    }

    /**
     * Show report creation form
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function createMedicalIncident(Request $request)
    {

        $this->restrictAccess();
        return view(
            'ambulance.medicalHistoryForm',
            [
                'patientIdentifier' => $request->get('patientIdentifier'),
            ]
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function addMedicalIncident(Request $request)
    {

        $this->restrictAccess();

        Validator::make(
            $request->all(),
            [
                'summary' => 'required|string|max:5120'
            ]
        )->validate();

        $incident = new MedicalHistory();
        $incident->user_identifier = $request->patientId;
        $incident->characterName = UserHelper::getCharacterName(User::find($request->patientId));
        $incident->ems_worker_id = Auth::user()->getFiveMUserData()->emsWorker()->first()->id;
        $incident->summary = $request->summary;
        $incident->save();


        return redirect(route('ambulanceSingleUser', ['steamId' => $request->patientId]));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function deleteMedicalIncident(Request $request)
    {

        $this->restrictAccess();
        $record = MedicalHistory::find($request->medicalIncidentId);

        if(Auth::user()->getFiveMUserData()->identifier !== $record->emsWorker()->first()->user_identifier) {
            return redirect()->back()->withErrors(['You have insufficient permissions to do that action']);
        }

        $userId = $record->user_identifier;
        $record->delete();

        return redirect()->route('ambulanceSingleUser', ['steamId' => $userId]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function showMedicalIncident($id)
    {

        $this->restrictAccess();
        return view('ambulance.medicalIncidentSummary', ['incident' => MedicalHistory::with(['user'])->find($id)]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showApplicationsList()
    {

        $this->restrictAccess();
        $this->restrictAccessForCaptain();

        return view(
            'common.jobApplicationsList',
            [
                'applications' => JobApplication::where('job', 'ems')
                    ->where('isAccepted', false)
                    ->where('isRejected', false)
                    ->paginate(10),
                'singleApplicationRoute' => 'emsShowSingleApplication'
            ]
        );
    }

    /**
     * @param Request $request
     * @param $applicationId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function showSingleApplication($applicationId)
    {

        $this->restrictAccess();
        $this->restrictAccessForCaptain();

        $application = JobApplication::with(['user'])->find($applicationId);
        $answers = json_decode($application->answers);
        $user = $application->user()->first();

        return view(
            'common.jobApplication',
            [
                'application' => $application,
                'answers' => $answers,
                'user' => $user,
                'acceptRoute' => 'emsAcceptApplication',
                'rejectRoute' => 'emsRejectApplication'
            ]
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function acceptApplication(Request $request)
    {

        $this->restrictAccess();
        $this->restrictAccessForCaptain();

        $application = JobApplication::find($request->id);
        $application->isAccepted = true;
        $application->isRejected = false;
        $application->bossIdentifier = Auth::user()->id;
        $application->save();

        return redirect(route('emsShowApplicationsList'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function rejectApplication(Request $request)
    {

        $this->restrictAccess();
        $this->restrictAccessForCaptain();

        $application = JobApplication::find($request->id);
        $application->isAccepted = false;
        $application->isRejected = true;
        $application->bossIdentifier = Auth::user()->id;
        $application->save();

        return redirect(route('emsShowApplicationsList'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function showAcceptedJobApplications()
    {

        $this->restrictAccess();
        $this->restrictAccessForCaptain();
        return view(
            'common.jobApplicationsList',
            [
                'applications' => JobApplication::with(['user'])->where('job', 'ems')
                    ->where('isAccepted', true)
                    ->where('isRejected', false)
                    ->where('initiated', false)
                    ->paginate(10),
                'singleApplicationRoute' => 'emsShowSingleAcceptedApplication'
            ]
        );
    }

    /**
     * @param Request $request
     * @param $applicationId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function showSingleAcceptedApplication($applicationId)
    {

        $this->restrictAccess();
        $this->restrictAccessForCaptain();

        $application = JobApplication::with(['user'])->find($applicationId);
        $answers = json_decode($application->answers);
        $user = $application->user()->first();

        return view(
            'common.jobApplication',
            [
                'application' => $application,
                'answers' => $answers,
                'user' => $user,
                'acceptRoute' => 'emsInitiateApplicantToWork',
                'acceptButtonName' => __('buttons.hire'),
                'rejectRoute' => 'emsRejectApplication'
            ]
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function initiateApplicantToWork(Request $request)
    {

        $this->restrictAccess();
        $this->restrictAccessForCaptain();

        $application = JobApplication::find($request->id);
        $application->isAccepted = true;
        $application->isRejected = false;
        $application->initiated = true;
        $application->bossIdentifier = Auth::user()->id;
        $application->save();

        $emsWorker = new EmsWorker();
        $emsWorker->user_identifier = $application->user_identifier;
        $emsWorker->is_hired = true;
        $emsWorker->save();

        return redirect(route('emsShowAcceptedJobApplications'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function showEmsWorkersList()
    {

        $this->restrictAccess();

        return view(
            'ambulance.allWorkers',
            [
                'users' => User::where('job', '=', 'ambulance')->orWhere('job', '=', 'offambulance')->get(),
                'ranks' => JobGrades::where('job_name', '=', 'ambulance')->get()
            ]
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function removeTattoos(Request $request)
    {

        $this->restrictAccess();

        $loggedInUserData =Auth::user()->getFiveMUserData();

        $isAdmin = Auth::user()->hasRole('admin');

        if (!in_array($loggedInUserData->job_grade, self::TATTOO_REMOVAL_ALLOWED_JOB_GRADES) and !$isAdmin) {
            abort(401, 'This action is unauthorized.');
        }

        $user = User::find($request->patientId);
        $user->tattoos = null;
        $user->save;

        return redirect()->back()->with('status', 'Tattoos Removed');

    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function allBills(Request $request)
    {

        $this->restrictAccess();

        return view(
            'common.bills',
            BillsHelper::getSocietyBills('society_ambulance')
        );
    }

    /**
     * Control User job in Server and if not ambulance, then dont allow to continue
     */
    protected function restrictAccess()
    {

        $loggedInUserData =Auth::user()->getFiveMUserData();

        $isAdmin = Auth::user()->hasRole('admin');
        if(!in_array($loggedInUserData->job, self::ALLOWED_JOBS) and !$isAdmin) {
            abort(401, 'This action is unauthorized.');
        }

        return;
    }

    /**
     *  Restrict user access only for certain job grades
     */
    protected function restrictAccessForCaptain()
    {

        $loggedInUserData = Auth::user()->getFiveMUserData();

        $isAdmin = Auth::user()->hasRole('admin');
        if (!in_array($loggedInUserData->job_grade, self::ALLOWED_JOB_GRADES) and !$isAdmin) {
            abort(401, 'This action is unauthorized.');
        }

        return;
    }
}
