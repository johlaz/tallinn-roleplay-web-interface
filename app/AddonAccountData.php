<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AddonAccountData extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'addon_account_data';
}
