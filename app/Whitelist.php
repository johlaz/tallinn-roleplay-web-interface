<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Whitelist extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'whitelist';

    /**
     * @var bool
     */
    public $timestamps = false;
}
