<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhoneAppChat extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'phone_app_chat';
}
